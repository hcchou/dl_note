# dl_note

1. Specify python version installation  

```
sudo python3.9 -m pip install torch==1.12.1+cu113 torchvision==0.13.1+cu113 torchaudio==0.12.1 --extra-index-url https://download.pytorch.org/whl/cu113
```

2. Specify target environment installation  
```
conda install -p ~/miniconda3/envs/deep_ev_tracker torch==1.12.1+cu113
```

3. Install package from local package  
```
conda install -p ~/miniconda3/envs/deep_ev_tracker --use-local opencv-python-headless-4.5.5.64-py39_0.tar.bz2
```

4. Create conda enviroment with specific python version
```
conda create -n deep_ev_tracker python=3.9.7
```

5. Specify python version installation and use requirement.txt
```
sudo python3.9 -m pip install -r requirements.txt
```